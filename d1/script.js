// console.log("Hello world");
// Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum); // --> Old way

const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);

// Template Literals
let name = "John";

// pre-template literal strings

let msg = "Hello "+ name+ "! Welcome to programming!"
console.log("Msg w/out template literals");

msg = `Hello ${name}! Welcome to programming!`
console.log(`Msg with template literals: ${msg}`);

const anotherMsg = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with a solution of ${firstNum}.
`
console.log(anotherMsg);

const interestRate = 0.1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

const fullName = ["Juan", "Dela", "Cruz"];
// Pre-Array Destructuring
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
//Using template literal
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

// Object Destructuring
const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
}
// Pre-destruturing
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good see you.`);

// obj destructuring
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

// Using var to a function
function getFullName({givenName, maidenName, familyName}){
    console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person)

// Arrow function
const hello = () => {
    console.log("Hello World!");
}
hello()

// Traditional Function

// function printFullName(firstName, middleName, lastName){
//     console.log(firstName + " " + middleName+ " " + lastName);
// }
// printFullName("John", "Doe", "Smith")

// Arrow Function
const printFullName = (firstName, middleName, lastName) => {
    console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName("John", "Doe", "Smith")

const students = ["John", "Jane", "Judy"]
// Arrow Function with loops
// Pre-Arrow function 

students.forEach(function(student){
    console.log(`${student} is a student`);
})

// Arrow function
students.forEach((student) => {
    console.log(`${student} is a student`);
});

// Default Function argument value
const greet = (name = "User") => {
    return `Good morning, ${name}`
}
console.log(greet());
console.log(greet("Lawrence"));


// Class-based Object blueprints
// Creating a class

class Car {
    constructor(brand, name, year){
        this.brand = brand,
        this.name = name,
        this.year = year
    }
}
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = "2021";
console.log(myCar);
// 
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);
// 





